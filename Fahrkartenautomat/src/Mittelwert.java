import java.util.Scanner;

public class Mittelwert {

	   public static void main(String[] args) {
		   
		  Scanner tastatur = new Scanner(System.in);
		   	
	      // (E) "Eingabe"
	      // Werte f�r x und y festlegen:
	      // ===========================
	      
		  double ersteZahl = 0;
		  double zweiteZahl = 0;
		  double mittelWert = 0;
		  
		  System.out.println ("Geben sie ihre erste Zahl an:");				
	      ersteZahl = tastatur.nextDouble();
	       
	      System.out.println ("Ihre erste Zahl lautet: " + ersteZahl);	
	      System.out.println ("Geben sie ihre zweite Zahl an:");				
		  zweiteZahl = tastatur.nextDouble();
	       
	      

	      // (A) Ausgabe
	      // Ergebnis auf der Konsole ausgeben:
	      // =================================	
	  
		  mittelWert = Mittelwert.mittelwertBerechnen(ersteZahl, zweiteZahl);
	    
	    
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", ersteZahl, zweiteZahl, mittelWert);
			
	   }
	   
	      
	      // (V) Verarbeitung
	      // Mittelwert von x und y berechnen: 
	      // ===============================

	      public static double mittelwertBerechnen(double ersteZahl,double zweiteZahl) {
	  	  double m = (ersteZahl + zweiteZahl) / 2.0;
		  return m;
						
		} 
	}