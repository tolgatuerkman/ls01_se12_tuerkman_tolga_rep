
public class AB_Ausgabeformatierung2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//AUfgabe 1, Muster ohne Leerzeichen nachbilden
		
		System.out.println("Aufgabe1 \n");
		
		// Die Werten x, s die Zeichen ** und * zuordnen mit String
		
		String x = "**";				
		String s = "*";
		
		// %50s steht f�r rechtsb�ndig mit 50 stellen
		// \n line Feed
		
		System.out.printf( "%50s \n", x );
		System.out.printf( "%46s ", s );
		System.out.printf( "%6s \n", s );
		System.out.printf( "%46s ", s );
		System.out.printf( "%6s \n", s );
		System.out.printf( "%50s \n", x );
		
		System.out.printf( "\n");
		
		//Aufgabe 2
		
		//ln f�r line Feed, \n auch f�r line feed -> 2 Zeilenumbr�che
		
		System.out.println("Aufgabe2 \n");
		
		// Die Buchstaben u-z Zahlenwerte zuordnen mit int
		
		int u = 1 ;
		int v = 1 ;
		int w = 2 ;
		int q = 6 ;
		int y = 24 ;
		int z = 120 ;
		
		String gleich = "=" ;
		
		
		System.out.printf( "0!");
		System.out.printf( "%3s", gleich );
		System.out.printf( "%20s", gleich );
		System.out.printf( "%4s\n", u ); 
		
		
		
		System.out.printf( "1!");
		System.out.printf( "%3s", gleich );
		System.out.printf( "%2s" , "1");
		System.out.printf( "%18s", gleich );
		System.out.printf( "%4s\n", v ); 
		
		System.out.printf( "2!");
		System.out.printf( "%3s", gleich );
		System.out.printf( "%6s" , "1 * 2");
		System.out.printf( "%14s", gleich );
		System.out.printf( "%4s\n", w ); 
		
		System.out.printf( "3!");
		System.out.printf( "%3s", gleich );
		System.out.printf( "%10s" , "1 * 2 * 3");
		System.out.printf( "%10s", gleich );
		System.out.printf( "%4s\n", q ); 
		
		System.out.printf( "4!");
		System.out.printf( "%3s", gleich );
		System.out.printf( "%14s" , "1 * 2 * 3 * 4");
		System.out.printf( "%6s", gleich );
		System.out.printf( "%4s\n", y ); 
		
		System.out.printf( "4!");
		System.out.printf( "%3s", gleich );
		System.out.printf( "%18s" , "1 * 2 * 3 * 4 * 5");
		System.out.printf( "%2s", gleich );
		System.out.printf( "%4s \n", z); 
		
		
		//Aufgabe 3
		
		System.out.printf( "\n");
		System.out.println("Aufgabe3 \n");
		
		
		System.out.print( "Fahrenheit");
		System.out.printf( "%2s", "|");
		System.out.printf( "%11s", "Celsius \n");
		
		System.out.printf( "---------------------- \n");
		
		System.out.printf("-20");
		System.out.printf( "%9s", "|");
		System.out.printf( "%9.2f \n" , -28.8889);
		
		System.out.printf("-10");
		System.out.printf( "%9s", "|");
		System.out.printf( "%9.2f \n" , -23.3333);
		
		System.out.printf("+0");
		System.out.printf( "%10s", "|");
		System.out.printf( "%9.2f \n" , -17.7778);
		
		System.out.printf("+20");
		System.out.printf( "%9s", "|");
		System.out.printf( "%9.2f \n" , -6.6667);
		
		System.out.printf("+30");
		System.out.printf( "%9s", "|");
		System.out.printf( "%9.2f \n" , -1.1111);
		
		
		
		
		
		
		
		
		
				
				
				
	}

}
