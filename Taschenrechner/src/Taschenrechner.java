// Tolga T�rkman
// Version 2.0 
// 17.11.2021
// Mit Hilfsmitteln aus dem Internet. Quelle: (https://studyflix.de/informatik/switch-case-java-1804, 17.11.2021)	

import java.util.Scanner;    //Scanner 

public class Taschenrechner {  //Class name Taschenrechner

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
			Scanner sc = new Scanner(System.in);	// Neue Scanner gesetzt
			double wert1 = 0;						// Variablendeklaration und Variableninitialisierung
			double wert2 = 0;						// Variablendeklaration und Variableninitialisierung

			
			System.out.println("Geben sie bitte eine Zahl ein:");   // Konsolenausgabe 
			wert1 = sc.nextDouble();								// Neues Startwert, altes wird �berschrieben
			
			System.out.println ("Ihre erste Zahl lautet = " + wert1); //Kosolenausgabe + Wert1 wiedergabe
			
			System.out.println("Geben sie bitte eine Zahl ein:");    //Konsolenausgabe 
			wert2 = sc.nextDouble();								// Neues Startwert, altes wird �berschrieben
			
			System.out.println ("Ihre zweite Zahl lautet = " + wert2);  //Kosolenausgabe + Wert2 wiedergabe
			
			System.out.println("M�chten sie addieren, subtrahieren, multiplizieren oder dividieren? ");		// Konsolenausgabe 
			System.out.println("F�r Addition, tippen Sie \"1\", f�r Subtraktion \"2\", f�r Multiplikation \"3\" und f�r Division \"4\". Best�tigen Sie mit Eingabe-Taste. "); //Switch-Case Vorbreitung 
			
			
			double ergebnis1 = wert1 + wert2;  //Switch Case Vorbreitung- Alle 4 Rechenarten mit Operatoren deklarisieren und initialisieren
			double ergebnis2 = wert1 - wert2;
			double ergebnis3 = wert1 * wert2;
			double ergebnis4 = wert1 / wert2;

			int wert3= sc.nextInt();		//Switch-Case �berpr�ft nur den int oder String Wert einer Variable, daher neues Wert deklasieiren und intialisieren und �bernehemn

			switch (wert3)					// (Switch=Zuweisen auf Case1-4) Mehrfach geschaltete If-Anweisung, hier wirds eingeleitet

			{
			case 1: System.out.printf("Das Ergebnis lautet = " + wert1 + " + " + wert2 + " = " + "%.2f \n", ergebnis1);break; //Case=Sprungziel, Anweisung 1, wenn man 1 ausw�hlt. Break verl�sst die Switch Anweisung
			case 2: System.out.printf("Das Ergebnis lautet = " + wert1 + " - " + wert2 + " = " + "%.2f \n" ,ergebnis2);break; //% Platzhalter, f�r ergebnis2, .2 zeigt ergebnis2 nur auf 2 Kommastellen an, f f�r Kommazahl, \n f�r Zeilenvorschub
			case 3: System.out.printf("Das Ergebnis lautet = " + wert1 + " * " + wert2 + " = " + "%.2f \n" ,ergebnis3);break;
			case 4: System.out.printf("Das Ergebnis lautet = " + wert1 + " / " + wert2 + " = " + "%.2f \n" ,ergebnis4);break;			
			
	}
}
}
