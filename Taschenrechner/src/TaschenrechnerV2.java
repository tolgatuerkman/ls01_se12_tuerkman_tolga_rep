// Tolga T�rkman
// Version 2.0 	

import java.util.Scanner;    //Scanner 

public class TaschenrechnerV2 {  //Class name Taschenrechner

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
			Scanner sc = new Scanner(System.in);	// Neue Scanner gesetzt
			
			
			double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;
			
			 TaschenrechnerV2.zahlenErfassen();		
			 
			 
		        zahl1 = TaschenrechnerV2.ersteEingabe(zahl1, sc);
		        zahl2 = TaschenrechnerV2.zweiteEingabe(zahl2, sc);

		        
		      TaschenrechnerV2.auswahl();
		      
		      erg = TaschenrechnerV2.ausgabe(zahl1, zahl2, erg, sc);
		      
		      
}
	
	 public static void zahlenErfassen() {
    	 System.out.println("Taschenrechner: ");
         System.out.println("Das Programm addiert, subtrahiert, dividiert oder multipliziert 2 eingegebene Zahlen. ");
	}
    
	 
	 public static void auswahl() {
		System.out.println("M�chten sie addieren, subtrahieren, multiplizieren oder dividieren? ");		 
		System.out.println("F�r Addition, tippen Sie \"1\", f�r Subtraktion \"2\", f�r Multiplikation \"3\", f�r Division \"4\" und f�r Modulo \"5\". Best�tigen Sie mit Eingabe-Taste. "); 
	}
	 
	 
    public static double ersteEingabe(double zahl1,Scanner sc) {
    	System.out.println("Geben Sie bitte ihre erste Zahl ein:");
    	zahl1 = sc.nextDouble();
    	System.out.println ("Ihre erste Zahl lautet = " + zahl1);
    	return zahl1;
    }
    	
    	
    
    public static double zweiteEingabe(double zahl2, Scanner sc) {
    	System.out.println("Geben Sie bitte ihre zweite Zahl ein:");
    	zahl2 = sc.nextDouble();
    	System.out.println ("Ihre zweite Zahl lautet = " + zahl2);
    	return zahl2;
    }
    
    
    
    public static double ausgabe(double zahl1, double zahl2, double erg, Scanner sc) {
		int wert3= sc.nextInt();		

		switch (wert3)					

		{
		case 1: 
			erg = zahl1 + zahl2;	
			System.out.printf("Das Ergebnis lautet = " + zahl1 + " + " + zahl2 + " = " + "%.2f \n", erg);break; 
		
		case 2: 
			erg = zahl1 - zahl2;
			System.out.printf("Das Ergebnis lautet = " + zahl1 + " - " + zahl2 + " = " + "%.2f \n" ,erg);break; 
		
		case 3: 
			erg = zahl1 * zahl2;
			System.out.printf("Das Ergebnis lautet = " + zahl1 + " * " + zahl2 + " = " + "%.2f \n" ,erg);break;
		
		
		case 4: 
			erg = zahl1 / zahl2;
			System.out.printf("Das Ergebnis lautet = " + zahl1 + " / " + zahl2 + " = " + "%.2f \n" ,erg);break;		
			
		case 5: 
			erg = zahl1 % zahl2;
			System.out.printf("Das Ergebnis lautet = " + zahl1 + " % " + zahl2 + " = " + "%.2f \n" ,erg);break;		
		
}
    	return erg;
    }
    
    
}
