// 10.12.2021
import java.util.Scanner;

class Addition {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;

        Addition.hinweis();							
        zahl1 = Addition.ersteEingabe(zahl1);
        zahl2 = Addition.zweiteEingabe(zahl2);
        
        erg = Addition.addieren(zahl1, zahl2);
        Addition.ausgabe(zahl1, zahl2, erg);
 }
    											  
    					
    public static void hinweis() {
    	 System.out.println("Hinweis: ");
         System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
	}
    
    public static double ersteEingabe(double zahl1) {
    	System.out.println(" 1. Zahl:");
    	zahl1 = sc.nextDouble();
    	return zahl1;
    }
    	
    	
    public static double zweiteEingabe(double zahl2) {
    	System.out.println(" 2. Zahl:");
    	zahl2 = sc.nextDouble();
    	return zahl2;
    }


    public static void ausgabe(double zahl1,double zahl2, double erg) {
    	System.out.println("Ergebnis der Addition");
        System.out.printf("%.2f + %.2f = %.2f", zahl1, zahl2, erg);
    }
    
    public static double addieren(double zahl1, double zahl2) {  
    	double erg = zahl1 + zahl2;								
    	return erg;												 
    }
    
}